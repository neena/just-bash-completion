#/usr/bin/env bash
_just_completions()
{
	if just -l >/dev/null 2>&1; then
		COMPREPLY=($(compgen -W "$(just -l | tail -n +2 | awk '{ print $1 }' | grep ^${COMP_WORDS[COMP_CWORD]})"))
	fi
}

complete -F _just_completions just
